const tabs = document.querySelectorAll('.tabs-title');
const contents = document.querySelectorAll('.tabs-content-item');

tabs.forEach((item, index) => {
    item.addEventListener('click', () => {

        for (let tab of tabs) {
			tab.classList.remove('active');
        };
        item.classList.add('active');

        contents.forEach(() => {       
        for (let content of contents) {
            content.classList.remove('active');
        };
        contents[index].classList.add('active');
        })   
    })
})

